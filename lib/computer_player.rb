class ComputerPlayer
  attr_accessor :name
  attr_accessor :board
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    board.grid.each{|row| puts row.to_s + "\n"}
  end

  def get_move
    win_on_this_move
  end

  def win_on_this_move
    (0..@board.grid.size - 1).each do |row|
      (0..@board.grid[row].size - 1).each do |i|
        board_clone = @board.clone()
        if board_clone.grid[row][i] == nil
          board_clone.place_mark([row, i], @mark)
        end
        if board_clone.won?
          return [i, row]
        end
      end
    end

    finished = false
    while !finished do
      random_move = [(@board.grid.size * rand), (@board.grid[@board.grid.size * rand].size * rand)]
      if @board.grid[random_move[0]][random_move[1]] == nil
        finished = true
        return random_move
      end
    end

  end

end
