class HumanPlayer
  attr_accessor :name
  attr_accessor :board
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    board.grid.each{|row| puts row.to_s + "\n"}
  end

  def get_move
    puts "type of move where format is like '0, 0'"
    input = gets.chomp
    array = input.gsub(/[,]/, "").split
    move = array.map{|i| i = i.to_i}
  end

end
