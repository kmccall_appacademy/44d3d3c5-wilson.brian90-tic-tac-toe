require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  @hum_sym = :X
  @comp_sym = :O
  attr_accessor :current_player

  attr_accessor :board

  def initialize(hum, comp)
    @board = Board.new
    @hum = hum
    @comp = comp
    @hum.mark = :X
    @comp.mark = :O
    @current_player = hum
  end

  def play
    while !@board.won? do
      play_turn
    end
  end

  def switch_players!
    if @current_player == @hum
      @current_player = @comp
    else
      @current_player = @hum
    end
  end

  def play_turn
    current_move = @current_player.get_move
    @board.place_mark(current_move, @current_player.mark)
    @current_player.display(@board)
    switch_players!
  end

end
