class Board
  attr_accessor :grid
  attr_accessor :winner

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    a = pos[0]
    b = pos[1]
    if @grid[a][b] != nil
      raise Exception.new("Invalid move")
    end
    @grid[a][b] = mark
    won?
  end

  def empty?(pos)
    a = pos[0]
    b = pos[1]
    if @grid[a][b] == nil
      return true
    end
    return false
  end

  def won?
    @grid.each do |row|
      if ((row[0] == row[1] && row[1] == row[2]) && row[0] != nil)
        @winner = row[0]
        return true
      end
    end

    (0..2).each do |col|
      if (@grid[0][col] == @grid[1][col]) && ((@grid[1][col] == @grid[2][col]) && (@grid[0][col] != nil))
        @winner = @grid[0][col]
        return true
      end
    end

    if (@grid[0][0] == @grid[1][1]) && (@grid[1][1] == @grid[2][2] && grid[0][0] != nil)
      @winner = @grid[0][0]
      return true
    end

    if (@grid[0][2] == @grid[1][1]) && (@grid[1][1] == @grid[2][0] && grid[0][2] != nil)
      @winner = @grid[0][2]
      return true
    end

    return false
  end

  def over?
    if won?
      return true
    elsif @grid.all?{|row| row.all?{|space| space != nil}}
      @winner = nil
      return true
    end
    return false
  end

end
